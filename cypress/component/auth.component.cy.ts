import {AuthComponent} from "../../src/app/auth/auth.component";
import {PlaceholderDirective} from "../../src/app/shared/placeholder/placeholder.directive";
import {AuthService} from "../../src/app/auth/auth.service";
import {MockAuthService, testAppRoutes} from "../../src/app/shared/test-helpers";
import {FormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {faker} from "@faker-js/faker";
import {TestBed} from "@angular/core/testing";
import {Router} from "@angular/router";

describe("AuthComponent", () => {
  it('should display a spinner component when loading is true', () => {
    cy.mount(AuthComponent, {
      imports: [FormsModule, RouterTestingModule.withRoutes(testAppRoutes)],
      declarations: [PlaceholderDirective],
      providers: [{provide: AuthService, useClass: MockAuthService}],
      componentProperties: {
        loginMode: false,
        loading: true,
        error: null,
      }
    });

    cy.get('app-loading-spinner').should('exist');
  })
  beforeEach(() => {
    cy.mount(AuthComponent, {
      imports: [FormsModule, RouterTestingModule.withRoutes(testAppRoutes)],
      declarations: [PlaceholderDirective],
      providers: [{provide: AuthService, useClass: MockAuthService}],
      componentProperties: {
        loginMode: false,
        loading: false,
        error: null,
      }
    })
      .then((authCmp) => {
        const router = TestBed.inject(Router);
        cy.spy(authCmp.component, 'onSubmit').as('onSubmitSpy');
        cy.stub(router, 'navigate').as('navigate')
        return cy.wrap(authCmp).as('angular');
      });
  })

  it("should have the button disabled if there form input is invalid", () => {
    // empty form should be invalid
    cy.get("[disabled]").should('be.disabled');

    //entering incorrect email, with correct password
    cy.get("#email").type('incorrect format');
    cy.get("#password").type(faker.internet.password())
    cy.get("[disabled]").should('be.disabled');

    // clear
    cy.get("#email").clear()
    cy.get("#password").clear()

    // entering a correct email format but not a long enough password
    cy.get("#email").type(faker.internet.email());
    cy.get("#password").type(faker.internet.password(5))// min password is of length 6
    cy.get("[disabled]").should('be.disabled');

    // clear
    cy.get("#email").clear()
    cy.get("#password").clear()

    // entering a correct email and password
    cy.get("#email").type(faker.internet.email());
    cy.get("#password").type(faker.internet.password(20))// min password is of length 6
    cy.get("#submit-button").should('not.be.disabled');
  })


  it("should create an alert component when someone tries to signup with an existing email", () => {
    // arrange
    let email = "incorrect@test.com"
    let password = faker.internet.password();

    // act
    cy.get("#email").type(email);
    cy.get("#password").type(password);
    cy.get('#submit-button').click();

    //assert
    cy.get(".alert-box").within(() => cy.get("p").should('have.text', 'This email already exists'))
  });

  it('should create an alert component when someone logs in with the incorrect password', () => {
    //arrange
    cy.get('#toggle-button').click();// making sure it's set to login
    let email = "incorrect@test.com"
    let password = faker.internet.password();

    //act
    cy.get("#email").type(email);
    cy.get("#password").type(password);
    cy.get('#submit-button').click();

    //assert
    cy.get(".alert-box").within(() => cy.get("p").should('have.text', 'Incorrect password'));
  })

  it("should route a successful login to /recipes", () => {
    //arrange
    cy.get('#toggle-button').click();// making sure it's set to login
    cy.get('#submit-button').should('have.text', "Login")
    let email = faker.internet.email();
    let password = faker.internet.password();

    //act
    cy.get("#email").type(email);
    cy.get("#password").type(password);
    cy.get('#submit-button').click();

    // assert
    cy.get('@navigate').should('have.been.calledWith', ['/recipes'])
  })

  it("should route a successful signup to /recipes", () => {
    //arrange
    let email = faker.internet.email();
    let password = faker.internet.password();
    cy.get('#submit-button').should('have.text', "Sign up")

    //act
    cy.get("#email").type(email);
    cy.get("#password").type(password);
    cy.get('#submit-button').click();

    // assert
    cy.get('@navigate').should('have.been.calledWith', ['/recipes'])
  })
})
