// function to check if an object implements the AuthResponseData interface
export const authResponseDataGuard = (object: unknown) => {
  return Object.prototype.hasOwnProperty.call(object, "kind")
    && Object.prototype.hasOwnProperty.call(object, "idToken")
    && Object.prototype.hasOwnProperty.call(object, "email")
    && Object.prototype.hasOwnProperty.call(object, "refreshToken")
    && Object.prototype.hasOwnProperty.call(object, "expiresIn")
    && Object.prototype.hasOwnProperty.call(object, "localId")
    // && Object.prototype.hasOwnProperty.call(object, "registered")
}

export const urls = {
  signup: "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo",
  login: 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo',
  saveData: 'https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json*',
};

