import {faker} from "@faker-js/faker"
import {urls} from "../utils";
import {Recipe} from "../../src/app/recipes/recipe.model";
import {User} from 'src/app/auth/user.model'

describe('New recipes', () => {
  const email = faker.internet.email();
  const password = faker.internet.password();
  beforeEach(() => {
    cy.addUser(email, password);
    cy.url().should('include', '/recipes')
  });

  /**
   * e2e test that tests the full life of a recipe component
   * including its creation, ability to be edited, to save data, and to be deleted
   */
  it(`should be created with the recipe edit component,
  added to the recipe list component, the ingredients should be able to be added to the shopping-list and persist,
   and a deleted recipe should be removed from the list`,
    () => {
      // new recipe info
      const amount = (Math.floor(Math.random() * 10) + 1);// random int > 0
      const recipe: Recipe = {
        name: faker.commerce.productName(),
        imagePath: faker.image.imageUrl(),
        description: faker.lorem.paragraph(1),
        ingredients: [{name: faker.commerce.product(), amount}],
      };
      let user: User = JSON.parse(localStorage.getItem('userData'));

      // interceptor for saving data
      cy.intercept({url: urls.saveData, method: 'PUT'}, (req) => {
        /**
          below are two options for using a deep equality chainer
         */
        expect(req.body[0]).to.eql(recipe);
        expect(req.body[0]).to.deep.eq(recipe)
      }).as('saveData')


      // creating a new recipe
      cy.get("[data-cy=new-recipe-button]").click()
      cy.get('form').within(() => {
        cy.get('#name').type(recipe.name);
        cy.get('#imagePath').type(recipe.imagePath);
        cy.get('#description').type(recipe.description)
        cy.get('[data-cy=addIngredientBtn]').click();
        cy.get('[formarrayname=ingredients]').within(() => {
          cy.get('[formcontrolname=name]').type(recipe.ingredients[0].name);
          cy.get('[formcontrolname=amount]').type(recipe.ingredients[0].amount.toString())
        });
        // submit the new recipe
        cy.get('[data-cy=submit-recipe-btn]').click();
      })// move outside the within

      // check that we've been routed back to the recipes page, and that the recipes exists within the recipe-list
      cy.url().should('include', '/recipes');
      cy.get(`[data-cy="${recipe.name.replaceAll(' ', '-').toLowerCase()}"]`)
        .should('exist').within(() => {
        // check that the edit component also works
        cy.get('a').click();
      })
      // add the ingredients from our new recipes to the shopping list
      cy.get('app-recipe-detail').should('exist').within(() => {
        cy.get('[data-cy=manage-btn]').click()
        cy.get('[data-cy=a-shopping-list]').click()
      })

      // navigate to the shopping list page
      cy.get("[data-cy=nav-shopping-list]").click()
      cy.url().should("include", 'shopping-list')

      // check that the ingredient in our new recipes persists in the shopping list
      cy.get('app-shopping-list').within(() => {
        cy.get(`[data-cy=${recipe.ingredients[0].name.replaceAll(' ', '-').toLowerCase()}]`).should('exist')
      })

      // save the data and ensure it persists
      cy.get('[data-cy=manage-dropdown]').click() //make sure the save data a elem is visible
      cy.get('[data-cy=save-data]').click()

      //remove the recipe from the list
      cy.get('[data-cy=nav-recipes]').click()
      cy.get(`[data-cy="${recipe.name.replaceAll(' ', '-').toLowerCase()}"]`)
        .within(() => {
          cy.get('a').click();
        })
      cy.get('app-recipe-detail').within(() => {
        cy.get('[data-cy=manage-btn]').click()
        cy.get('[data-cy=a-delete-recipe]').click()
      })

      cy.get('app-recipe-detail').should('not.exist');
      cy.get(`[data-cy="${recipe.name.replaceAll(' ', '-').toLowerCase()}"]`).should('not.exist')
    })
})
