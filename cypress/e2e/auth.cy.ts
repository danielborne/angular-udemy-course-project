import {faker} from "@faker-js/faker";
import {authResponseDataGuard, urls} from "../utils"
import {getAuth} from "firebase/auth";

describe('Signup', () => {
  beforeEach(() => {
    cy.startFirebase();
    cy.visit('/');
    cy.url().should('include', '/auth');
  });

  it('should send correct user data via post request to Firebase which will save a new user', () => {
    // arrange
    const email = faker.internet.email()
    const password = faker.internet.password(20)
    cy.get('[data-cy=email]').type(email);
    cy.get('[data-cy=password]').type(password);
    cy.get('#toggle-button').click()
    cy.get('[data-cy=submit-button]').should('have.text', "Sign up");
    cy.intercept({url: urls.signup, method: "POST"}, (req) => {
      req.continue((res) => {
        expect(authResponseDataGuard(res.body)).to.be.true;
      });
    }).as('signUp');

    // act
    cy.get('[data-cy=submit-button]').click();

    // assert
    cy.url().should("include", '/recipes', () => {
      // the above callback is similar to chaining a then
      // it ensures the check on the localStorage below is run after all the enqueued cy.Commands
      const userData = window.localStorage.getItem('userData');
      expect(authResponseDataGuard(userData)).to.be.true;
      //checking that the new user persists
      const auth = getAuth()
      expect(auth.currentUser.uid).to.be.truthy;
    });
  });

  it('should have firebase send an error if a user tries to sign up with an existing email address', () => {
    // arrange
    const email = faker.internet.email()
    const password = faker.internet.password(20)
    cy.addUser(email, password);
    cy.get('[data-cy=logoutElem]').click(); //making sure the session storage was cleared
    cy.visit('/auth');
    cy.get('#toggle-button').click();
    cy.get('[data-cy=submit-button]').should('have.text', "Sign up");
    cy.get('[data-cy=email]').type(email);
    cy.get('[data-cy=password]').type(password);
    cy.intercept({url: urls.signup, method: "POST"}, (req) => {
      req.continue((res) => {
        // expect(res.body.error.code).to.eq(400);
        // expect(res.body.error.message).to.eq('EMAIL_EXISTS');
      });
    }).as('signUpFail');

    // act
    cy.get('[data-cy=submit-button]').click();
    cy.wait('@signUpFail')

    //assert
    cy.get('.alert-box').within(() => {
      cy.get('p').should('have.text', "This email already exists")
    })
  });
});

describe("login", () => {
  const email = faker.internet.email();
  const password = faker.internet.password(20);

  beforeEach(() => {
    cy.startFirebase();
    cy.addUser(email, password);
    cy.visit('/auth');
    cy.get('[data-cy=submit-button]').should('have.text', "Login");
  })

  it('should send correct user data via post request to Firebase which will login an existing user', () => {
    // arrange
    cy.intercept({url: urls.login, method: "POST"}, (req) => {
      req.continue((res) => {
        expect(authResponseDataGuard(res.body)).to.be.true;
      });
    }).as('login');
    cy.get("[data-cy=email]").type(email);
    cy.get('[data-cy=password]').type(password);

    //act
    cy.get('[data-cy=submit-button]').click();

    //assert
    cy.url().should("include", '/recipes', () => {
      const userData = window.localStorage.getItem('userData');
      expect(authResponseDataGuard(userData)).to.be.true;
    });
  })

  it('should display an error message when the incorrect password is provided', () => {
    //arrange
    cy.intercept({url: urls.login, method: "POST"}, (req) => {
      req.continue((errorRes) => {
        expect(errorRes.body.error.code).to.eq(400);
        expect(errorRes.body.error.message).to.eq('INVALID_PASSWORD')
      })
    }).as('loginFail');
    cy.get("[data-cy=email]").type(email);
    cy.get('[data-cy=password]').type("incorrectpassword");

    // act
    cy.get('[data-cy=submit-button]').click();
    cy.wait('@loginFail')

    //assert
    cy.get('.alert-box').within(() => {
      cy.get('p').should('have.text', "Incorrect password")
    })
  })
})
