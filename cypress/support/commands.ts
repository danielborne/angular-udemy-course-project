import {connectDatabaseEmulator, getDatabase} from "@firebase/database";
import {
  connectAuthEmulator, getAuth,
} from "firebase/auth"
import {initializeApp} from "@firebase/app";
import {firebaseConfig} from "../../firebase/firebaseConfig";

/**
 * Spins up an instance of firebase to be used and torn down during testing
 * Firebase emulators need to be running in the terminal and the following code will ensure that
 * the requests made to firebase are intercepted
 */
Cypress.Commands.add('startFirebase', () => {
  const app = initializeApp(firebaseConfig);
  const auth = getAuth()
  const db = getDatabase()

  try {
    connectDatabaseEmulator(db, 'localhost', 9000);
    connectAuthEmulator(auth, 'http://localhost:9099')
  } catch (e) {
    console.log(e)
  }
  return {auth: auth, db: db};
});

/**
 * addUser will spin up a new database instance, add a user, and log them in.
 * This method should do so programmatically and not through the use of UI (you should test the login through your UI once though)
 * For the sake of time I did not do this programmatically since the backend used for the project is firebase
 */
Cypress.Commands.add('addUser', (email: string, password: string) => {
  const {auth, db} = cy.startFirebase();
  cy.visit('/auth');
  cy.get('[data-cy=email]').type(email);
  cy.get('[data-cy=password]').type(password);
  cy.get('#toggle-button').click()
  cy.get('[data-cy=submit-button]').should('have.text', "Sign up");
  cy.get('[data-cy=submit-button]').click();
})

