import {NgModule} from "@angular/core";
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";

export const appRoutes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: "full"},
  // load children needs #<class name> to find the right module at the path, new syntax requires the import function and anonymous function
  // {path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule'},
  {path: 'recipes', loadChildren: () => import('./recipes/recipes.module').then(mod => mod.RecipesModule) },
  {path: 'auth', loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule) },
  {path: 'shopping-list', loadChildren: () => import('./shopping-list/shopping-list.module').then(mod => mod.ShoppingListModule) },
]


@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],// preloading all models will have the rest of the components loaded while the users is browsing the page
  exports: [RouterModule]
})
export class AppRoutingModule {

}
