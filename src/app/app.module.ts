import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {provideFirebaseApp} from "@angular/fire/app";

import {AppComponent} from './app.component';
import {HeaderComponent} from "./header/header.component";
import {AppRoutingModule} from "./app-routing.module";
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core.module";
import {db, firebaseApp} from "../../firebase/firebaseConfig";
import {connectDatabaseEmulator, provideDatabase} from "@angular/fire/database";
import {connectAuthEmulator, initializeAuth, provideAuth, browserSessionPersistence} from "@angular/fire/auth";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    CoreModule,
    provideFirebaseApp(() => firebaseApp),
    provideDatabase(() => {
      connectDatabaseEmulator(db, 'localhost', 9000)
      return db
    }),
    provideAuth(() => {
      const auth = initializeAuth(firebaseApp, {persistence: browserSessionPersistence})
      connectAuthEmulator(auth, 'localhost:9099')
      return auth
    })
  ],
  providers: [CoreModule],
  bootstrap: [AppComponent]
  // use entry components for interacting with the dom to create components in angular versions older than 9
})
export class AppModule {
}
