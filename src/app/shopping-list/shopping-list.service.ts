import {Ingredient} from "../shared/ingredient.model";
import {Subject} from "rxjs";

export class ShoppingListService {
  ingredientsChanged = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();

  private shoppingList: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('bananas', 10)
  ];

  constructor() {
  }

  getShoppingList() {
    return this.shoppingList.slice();
  }

  append(ingredients: Ingredient[]) {
    this.shoppingList.push(...ingredients);
    this.ingredientsChanged.next(this.shoppingList.slice());
  }

  delete(ingredient: Ingredient) {
    this.shoppingList = this.shoppingList.filter(filterIngredient => filterIngredient !== ingredient);
  }

  getIndredient(index: number) {
    return this.shoppingList[index];
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.shoppingList[index] = newIngredient;
    this.ingredientsChanged.next(this.shoppingList.slice());
  }

  deleteIngredient(index: number) {
    this.shoppingList.splice(index, 1);
    this.ingredientsChanged.next(this.shoppingList.slice());
  }
}
