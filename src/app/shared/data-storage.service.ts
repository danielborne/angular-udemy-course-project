import {Injectable} from '@angular/core';
import {HttpClient } from "@angular/common/http";
import {RecipeService} from "../recipes/recipe.service";
import {Recipe} from "../recipes/recipe.model";
import {map, tap} from "rxjs/operators";
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  userSub: Subscription;

  constructor(private http: HttpClient, private recipeService: RecipeService,) {
  }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.http.put('https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json', recipes)
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchData() {
    // take operator takes one user from subject and then immediately unsubscribes
    // exhaustMap waits for the observable to complete then maps a new observable over it based on the logic in the function
    return this.http.get<Recipe[]>('https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(recipes => {
        return recipes.map(recipe => {
          return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
        })
      }), tap(recipes => {
        this.recipeService.setRecipes(recipes);
      }));
  }
}
