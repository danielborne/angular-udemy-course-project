import {Component} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {PlaceholderDirective} from "./placeholder.directive";
import {By} from "@angular/platform-browser";

@Component({selector: 'placeholder-parent',
template: `
    <ng-template appPlaceholder>
      <div class="parent-wrapper">
      </div>
    </ng-template>
`})
export class PlaceholderParent {}

describe('PlaceholderDirective', () => {
  let fixture: ComponentFixture<PlaceholderParent>;
  let parentCmp: PlaceholderParent;
  let directive;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlaceholderParent, PlaceholderDirective]
    }).compileComponents()
    fixture = TestBed.createComponent(PlaceholderParent);
    parentCmp = fixture.componentInstance;
    fixture.detectChanges();
    directive = fixture.debugElement.queryAllNodes(By.directive(PlaceholderDirective))[0];
  });

  it('should create an instance of the ViewContainerRef', () => {
    expect(directive).toBeTruthy();
  })
})
