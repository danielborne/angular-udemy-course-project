import {Directive, ElementRef, HostBinding, HostListener} from "@angular/core";
// only works by clicking the dropdown button a second time
// @Directive({
//   selector: '[appDropdown]'
// })
// export class DropdownDirective {
//   @HostListener('click') toggleOpen() {
//     this.isOpen = !this.isOpen
//   }
//   @HostBinding('class.open') isOpen: boolean = false;
//
//   constructor() {
//   }
// }

// implementation that allows you to close the dropdown by clicking anywhere else in the dom
@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen: boolean = false;
  @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
    this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false;
  }

  constructor(private elRef: ElementRef) {
  }
}
