import {User} from "../auth/user.model";
import {DebugElement, Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {Routes} from "@angular/router";
import {RecipesComponent} from "../recipes/recipes.component";
import {AuthComponent} from "../auth/auth.component";
import {ShoppingListComponent} from "../shopping-list/shopping-list.component";
import {HttpClient} from "@angular/common/http";
import {AuthGuard} from "../auth/auth.guard";
import {RecipeStartComponent} from "../recipes/recipe-start/recipe-start.component";
import {RecipeEditComponent} from "../recipes/recipe-edit/recipe-edit.component";
import {RecipeDetailComponent} from "../recipes/recipe-detail/recipe-detail.component";
import {RecipesResolverService} from "../recipes/recipes-resolver.service";

export const authResponseDataGuard = (object: unknown) => {
  return Object.prototype.hasOwnProperty.call(object, "kind")
    && Object.prototype.hasOwnProperty.call(object, "idToken")
    && Object.prototype.hasOwnProperty.call(object, "email")
    && Object.prototype.hasOwnProperty.call(object, "refreshToken")
    && Object.prototype.hasOwnProperty.call(object, "expiresIn")
    && Object.prototype.hasOwnProperty.call(object, "localId")
  // && Object.prototype.hasOwnProperty.call(object, "registered")
}

export const testAppRoutes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: "full"},
  {path: 'recipes', component: RecipesComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', component: RecipeStartComponent},
      {path: 'new', component: RecipeEditComponent},
      {path: ':id', component: RecipeDetailComponent, resolve: [RecipesResolverService]},
      {path: ':id/edit', component: RecipeEditComponent, resolve: [RecipesResolverService]},
    ]
  },
  {path: 'auth', component: AuthComponent},
  {path: 'shopping-list', component: ShoppingListComponent},
]

export const ButtonClickEvents = {
  left: {button: 0},
  right: {button: 2}
}

/** Simulate element click. Defaults to mouse left-button click event. */
export function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
  if (el instanceof HTMLElement) {
    el.click();
  } else {
    el.triggerEventHandler('click', eventObj);
  }
}

// data copied from an incorrect request to the firebase api
export const expectedLoginAndSignupError = {
  error: {
    code: 400,
    message: "INVALID_EMAIL",
    errors: [
      {
        message: "INVALID_EMAIL",
        domain: "global",
        reason: "invalid"
      }
    ]
  }
};

export const expectedLoginSuccess = {
  kind: "identitytoolkit#VerifyPasswordResponse",
  localId: "IsIpubxzRxdQYLASl7xjlpdPnxl2",
  email: "test@test.com",
  displayName: "",
  idToken: "eyJhbGciOiJSUzI1NiIsImtpZCI6IjVhNTA5ZjAxOWY3MGQ3NzlkODBmMTUyZDFhNWQzMzgxMWFiN2NlZjciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYW5ndWxhci1jb3Vyc2UtcHJvamVjdC0xNDE4MCIsImF1ZCI6ImFuZ3VsYXItY291cnNlLXByb2plY3QtMTQxODAiLCJhdXRoX3RpbWUiOjE2NzYwNTU2NTQsInVzZXJfaWQiOiJJc0lwdWJ4elJ4ZFFZTEFTbDd4amxwZFBueGwyIiwic3ViIjoiSXNJcHVieHpSeGRRWUxBU2w3eGpscGRQbnhsMiIsImlhdCI6MTY3NjA1NTY1NCwiZXhwIjoxNjc2MDU5MjU0LCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidGVzdEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.WAaTZ4klUH3WNrfaF3pfPysGtAVldtnly9lR5mveJIk9ThURCrmGfPQuNlNJI-K44kbWWdydvRYy4xUixfTBAusikrQ3fRVppjnK-vtfnT_65g0alZOifXo7-8RgWVnpoqryxNVimrAKPbon0T_mP7LS0BjmSaF5GRD8RzIxhTO9AmV1Jhyjn-qrxaWW3OArXvb0qJeQ63YIib-B4Bj8LDk2Lk0a1PxDdN2UsPfOxw-gMv1RDTlMFg88X1vkB-0UFBp2qfLdCxW-rKwzX9mFu94FzVQwgjqg2i1g6SK_lge3bPOWIkd6WpiYokz1Ky_2UrzWTT1pZPmENN2pAWt0sg",
  registered: true,
  refreshToken: "APJWN8cjoGjXOd9Qh5llpgD6oul7V5Qht5UHlpvdUkK-6gSM6ZFjtp9XuxfMlRZinvtEJHq0zlbBaBbSdATKopZYJC2Zkd3UHtnC-f0ePGj44j3akCbzg3TW77aQNjdlCCKEIlIBdJlTQnljejKaBrtN9N-vuXaJ8uTJ4u1PZUmGmMufYLkGuXp8nayFiN7JshAsyLneWli54dgDYH_gZBP6f0v8C_XFotl9w6cfgqz2_F4E908NQ4s",
  expiresIn: "3600"
}

const testExpirationDate = new Date();
testExpirationDate.setDate(testExpirationDate.getDate() + 1);
export const exampleUser = new User("test@test.com",
  "IsIpubxzRxdQYLASl7xjlpdPnxl2",
  "eyJhbGciOiJSUzI1NiIsImtpZCI6IjVhNTA5ZjAxOWY3MGQ3NzlkODBmMTUyZDFhNWQzMzgxMWFiN2NlZjciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYW5ndWxhci1jb3Vyc2UtcHJvamVjdC0xNDE4MCIsImF1ZCI6ImFuZ3VsYXItY291cnNlLXByb2plY3QtMTQxODAiLCJhdXRoX3RpbWUiOjE2NzU5NTY1MzcsInVzZXJfaWQiOiJJc0lwdWJ4elJ4ZFFZTEFTbDd4amxwZFBueGwyIiwic3ViIjoiSXNJcHVieHpSeGRRWUxBU2w3eGpscGRQbnhsMiIsImlhdCI6MTY3NTk1NjUzNywiZXhwIjoxNjc1OTYwMTM3LCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidGVzdEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.XTAK0DqijAgjCY2n6rB7WmnfhVngn0i3ecNB0Sqf0x0O4XJmnbRUbqbcE47enJqpFS-OOvUVS4s2A3vuuxGD4payt1C07a3OCQ7EaJUkZ1x7zf1143ceuLCREzw9dk4vrDKyEfLXYxkK5x5vc1q44CFUQgfAseFusji-DkJm_2em8-d3zrb7JyXevcilzAoYAxJuLmppe9-I8a7CmXirPBFDjHNdiZKEtooVsHBIwSyopUXc3c3S7-UZmSZ186t6EGnH2YNcqEWQ3Pj8JeekF4yeMVR2o1tvSPW1648TTIJvUOb3aMivSeoCkWQBeSytOLcRPXD9sG0xG9X_3ppXNQ",
  testExpirationDate); // set the expiration time to tomorrow


export class MockAuthService {
  user = new BehaviorSubject<User>(null);
  login = ((email: string, _password: string) => {
    if (email === "incorrect test") {
      return new Observable(subscriber => {
        subscriber.error('Incorrect password');
      })
    } else if (email === 'incorrect@test.com') {
      return new Observable(subscriber => {
        subscriber.error('Incorrect password')
      })
    }
    expectedLoginSuccess.email = email;
    this.user.next(exampleUser);// need to set a user so the auth guard doesn't block the test
    return new Observable(subscriber => {
      subscriber.next(expectedLoginSuccess)
    })
  })

  signup(email: string, _password: string) {
    if (email === 'incorrect test') {
      return new Observable(subscriber => {
        subscriber.error('This email already exists');
      })
    } else if (email === 'incorrect@test.com') {
      return new Observable(subscriber => {
        subscriber.error('This email already exists')
      })
    }
    this.user.next(exampleUser);
    expectedLoginSuccess.email = email;
    this.user.next(exampleUser);// need to set a user so the auth guard doesn't block the test
    return new Observable(subscriber => {
      subscriber.next(expectedLoginSuccess)
    })
  }

  autoLogin() {
    const userData: {
      email: string,
      id: string,
      _token: string,
      _tokenExpirationDate: string,
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }

    const loadedUser = new User(userData.email, userData.id, userData._token,
      new Date(userData._tokenExpirationDate));

    if (loadedUser.token) {
      this.user.next(loadedUser);
      const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  autoLogout(_expirationDuration) {
    return;
  }
}

@Injectable()
export class MockDataStorageService {
  constructor(private http: HttpClient) {
  }
  fetchData = () => {
    return this.http.get<any>('https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json');
  }
}
