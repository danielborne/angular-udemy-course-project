import {AuthService} from "./auth.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {AuthInterceptorService} from "./auth-interceptor.service";
import {exampleUser, MockAuthService, MockDataStorageService, testAppRoutes} from "../shared/test-helpers";
import {RouterTestingModule} from "@angular/router/testing";
import {HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {DataStorageService} from "../shared/data-storage.service";

describe("Auth-interceptor", () => {
  let httpCtrl: HttpTestingController;
  let authService: AuthService;
  let dataStorageService: DataStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true},
        {provide: AuthService, useClass: MockAuthService},
        {provide: DataStorageService, useClass: MockDataStorageService}
      ],
      imports: [RouterTestingModule.withRoutes(testAppRoutes), HttpClientTestingModule]
    }).compileComponents();
    httpCtrl = TestBed.inject(HttpTestingController);
    dataStorageService = TestBed.inject(DataStorageService);
    authService = TestBed.inject(AuthService);
  });

  afterEach(() => {
    httpCtrl.verify();
    jest.clearAllMocks();
  });

  it('should add the users token to each request made if the user is logged in', () => {
    localStorage.setItem('userData', JSON.stringify(exampleUser));
    authService.autoLogin();
    dataStorageService.fetchData().subscribe(resData => {
      expect(resData).toBeTruthy()
    });
    let req = httpCtrl.expectOne(`https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json?auth=${authService.user.value.token}`);
    expect(req.request.params.get('auth')).toBe(authService.user.value.token);
    req.flush(null);
  })

  it('should not add an auth token if no user is logged in', () => {
    localStorage.removeItem('userData');
    expect(authService.autoLogin()).toBeUndefined();
    dataStorageService.fetchData().subscribe(resData => {
      expect(resData).toBeTruthy()
    });
    expect(httpCtrl.expectNone(`https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json?auth=${exampleUser.token}`)).toBeFalsy();
    let req = httpCtrl.expectOne(`https://angular-course-project-14180-default-rtdb.firebaseio.com/recipes.json`);
    expect(req.request.params.get('auth')).toBeFalsy();
    req.flush(null);
  })
})
