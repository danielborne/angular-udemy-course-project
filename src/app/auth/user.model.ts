export class User {
  constructor(public email: string,
              public id: string,
              public _token: string,
              private _tokenExpirationDate: Date) {}

  get token() {
    // check if they have a valid token
    if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate) {
      return null;
    }
    return this._token;
  }
}
