import {TestBed} from "@angular/core/testing";
import {AuthGuard} from "./auth.guard";
import {AuthService} from "./auth.service";
import {exampleUser, MockAuthService, testAppRoutes} from "../shared/test-helpers";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: AuthService;
  let router: Router;
  let routerSpy: jest.SpyInstance;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(testAppRoutes)],
      providers: [AuthGuard, {provide: AuthService, useClass: MockAuthService},
        // {provide: Router, useClass: {navigate: () => null}}
      ]
    }).compileComponents();
    guard = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router)
    routerSpy = jest.spyOn(router, 'navigate');
  })

  it('should allow logged in users to the /recipes url tree', () => {
    authService.user.next(exampleUser);
    let snapshot = router.routerState.snapshot;
    expect(guard.canActivate(snapshot.root, snapshot)).toBeTruthy()
  });
});
