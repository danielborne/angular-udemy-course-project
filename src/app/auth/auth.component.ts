// import {ComponentFactoryResolver} from '@angular/core'
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService, AuthResponseData} from "./auth.service";
import {Observable, Subscription} from "rxjs";
import {Router} from "@angular/router";
import {AlertComponent} from "../shared/alert/alert.component";
import {PlaceholderDirective} from "../shared/placeholder/placeholder.directive";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ["./auth.component.css"],
})
export class AuthComponent implements OnDestroy, OnInit {
  loginMode = true;
  loading = false;
  error: string = null;
  // @viewchild will look for a child with the directive of the type in the parameter
  @ViewChild(PlaceholderDirective) alertHost: PlaceholderDirective;

  private closeSub: Subscription;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // ensuring the close subscription is closed
    if (this.closeSub) {
      this.closeSub.unsubscribe();
    }
  }

  switchLoginMode() {
    this.loginMode = !this.loginMode;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    let authObs: Observable<AuthResponseData>;

    this.loading = true;
    if (this.loginMode) {
      // login
      authObs = this.authService.login(email, password);
    } else {
      // signup
      authObs = this.authService.signup(email, password);
    }
    authObs.subscribe(resData => {
      this.loading = false;
      this.router.navigate(['/recipes']);
    }, errorMessage => {
      this.error = errorMessage;
      this.showErrorAlert(errorMessage);
      this.loading = false
    });

    form.reset();
  }

  onHandleError() {
    this.error = null;
  }

  private showErrorAlert(message: string) {
    // use a component factory for anything older than angular 9
    // const alertCmpFactory = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);
    const hostViewContainerRef = this.alertHost.viewContainerRef;
    hostViewContainerRef.clear();

    const cmpRef = hostViewContainerRef.createComponent(AlertComponent);
    cmpRef.instance.message = message;
    this.closeSub = cmpRef.instance.close.subscribe(() => {
      this.closeSub.unsubscribe();
      hostViewContainerRef.clear();
    }); // rare instance where you want to subscribe manually to an event emitter
  }
}
