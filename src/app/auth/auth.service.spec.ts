import {AuthService} from "./auth.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {fakeAsync, flush, TestBed, tick} from "@angular/core/testing";
import {Router} from "@angular/router";
import {faker} from "@faker-js/faker";
import {Location} from "@angular/common";
import {RouterTestingModule} from "@angular/router/testing";
import {authResponseDataGuard, exampleUser, expectedLoginAndSignupError, testAppRoutes} from "../shared/test-helpers";

describe('AuthService', () => {
  let authService: AuthService;
  let httpCtrl: HttpTestingController;
  let router: Router;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(testAppRoutes)],
      providers: [AuthService],
    });
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    httpCtrl = TestBed.inject(HttpTestingController);
    authService = TestBed.inject(AuthService);
    router.initialNavigation();
  });

  afterEach(() => {
    httpCtrl.verify();
    jest.clearAllMocks();
  });

  describe('Signup', () => {
    it('should post to firebase, and show an error message when incorrect formatting is submitted', () => {
      //arrange
      const email = "incorrectFormat";
      const password = "incorrectFormat";

      //act
      authService.signup(email, password)
        .subscribe(resData => {
          expect(resData).toEqual('Email taken, or provided invalid formatting');
        });
      let req = httpCtrl.expectOne('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo');

      //assert
      expect(req.request.method).toEqual('POST');
      expect((req.request.body)).toEqual({
        email: "incorrectFormat",
        password: "incorrectFormat",
        returnSecureToken: true
      })
      expectedLoginAndSignupError.error.message = "INVALID_EMAIL";
      req.flush(expectedLoginAndSignupError);
    });

    it('should return an error message when an existing user\'s email is provided', () => {
      const email = 'test@test.com';
      const password = 'wrongpassword';
      authService.login(email, password)
        .subscribe(resData => {
          expect(resData).toBe("This email already exists");
        });
      let req = httpCtrl.expectOne('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo');
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual({email: email, password: password, returnSecureToken: true});
      expectedLoginAndSignupError.error.message = "EMAIL_EXISTS";
      req.flush(expectedLoginAndSignupError);
    });

    it('should return a 200 response code with JSON that implements the AuthResponseData interface', () => {
      const email = faker.internet.email();
      const password = faker.internet.password();
      authService.signup(email, password)
        .subscribe(resData => {
          expect(resData).toBeTruthy();
        })
      let req = httpCtrl.expectOne('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo');
      expect(req.request.method).toEqual('POST');
      expect(req.request.body.email).toBe(email);
      expect(req.request.body.password).toBe(password);
      req.flush(exampleUser);
    });
  });

  describe("Login", () => {
    it('should return 200 with an AuthResponseDataType, when correct user data is entered', function () {
      const email = 'test@test.com';
      const password = 'test@test.com';
      authService.login(email, password)
        .subscribe(resData => {
          expect(authResponseDataGuard(resData)).toBe(true);
        })
      let req = httpCtrl.expectOne('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo');
      expect(req.request.method).toEqual('POST');
      req.flush(req.request.body);
    });

    it('should return an error message from the error handler when incorrect information is provided', () => {
      const email = faker.internet.email();
      const password = faker.internet.password();
      authService.login(email, password)
        .subscribe(resData => {
          expect(resData).toBe("INVALID_EMAIL");
        });
      let req = httpCtrl.expectOne('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo');
      expect(req.request.method).toEqual('POST');
      req.flush(expectedLoginAndSignupError);
    });
  });

  describe('autoLogin', () => {
    it('should return undefined if there is no user data in local storage', () => {
      localStorage.removeItem('userData');
      expect(authService.autoLogin()).toBeUndefined();
    });

    it('should trigger the user subject, and autoLogout method when a user exists in localStorage', () => {
      localStorage.setItem('userData', JSON.stringify(exampleUser));
      authService.autoLogin();
      expect(authService.user.value.email).toEqual(exampleUser.email);
      expect(authService.user.value.token).toEqual(exampleUser.token);
      expect(authService.user.value.id).toEqual(exampleUser.id);
    });
  });

  describe('logout', () => {
    it('should delete the localStorage userData, and user subject on a successful logout', () => {
      const routerSpy = jest.spyOn(router, 'navigate');
      localStorage.setItem('userData', JSON.stringify(exampleUser));
      // calling autoLogin to ensure the user subject is used
      authService.autoLogin();
      authService.logout();
      expect(authService.user.value).toBeNull();
      expect(localStorage.getItem('userData')).toBeNull();
      expect(routerSpy).toBeCalledWith(["/auth"]);
    });

    it('should fail if no user is logged in and the method is called', () => {
      localStorage.removeItem('userData');
      expect(authService.logout()).toBeUndefined();
    });
  });

  describe('autoLogout', () => {
    it('should logout the user and remove their data from localStorage and user subject after the duration expires', fakeAsync(() => {
      localStorage.setItem('userData', JSON.stringify(exampleUser));
      authService.autoLogin();// calling login to ensure the user subject is initialized
      authService.autoLogout(1);
      tick(1);// tick moves time forward
      expect(authService.user.value).toBeNull();
      expect(localStorage.getItem('userData')).toBeNull();
      flush(); // flush moves time to the end, and ensures timers are cleared
    }))
  })
});
