import {AuthComponent} from "./auth.component";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {AuthService} from "./auth.service";
import {click, MockAuthService, testAppRoutes} from "../shared/test-helpers";
import {FormsModule, NgForm} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {Router} from "@angular/router";
import {By} from "@angular/platform-browser";
import {PlaceholderDirective} from "../shared/placeholder/placeholder.directive";
import {of} from "rxjs";

describe('AuthCmp', () => {
  let authCmp: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;
  let authService: AuthService;
  let router: Router;

  //compile components in testbed
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuthComponent, PlaceholderDirective],
      imports: [FormsModule, RouterTestingModule.withRoutes(testAppRoutes)],
      providers: [{provide: AuthService, useClass: MockAuthService}]
    }).compileComponents();
    fixture = TestBed.createComponent(AuthComponent);
    authCmp = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    router.initialNavigation();
    fixture.detectChanges();// ensure data binding
  }));

  it('should initialize', () => {
    expect(authCmp).toBeTruthy();
  });

  it('should toggle between login and signup on switchLoginMode', () => {
    let toggleButtonEl = fixture.nativeElement.querySelector('#toggle-button');
    let submitButtonEl = fixture.nativeElement.querySelector('#submit-button');

    // check the defaults
    expect(submitButtonEl.textContent).toContain("Login");
    expect(toggleButtonEl.textContent).toContain("Switch to sign up");
    //toggle via click
    click(toggleButtonEl);
    fixture.detectChanges();
    expect(submitButtonEl.textContent).toContain("Sign up");
    expect(toggleButtonEl.textContent).toContain("Switch to login");
    //toggle via props
    authCmp.switchLoginMode();
    fixture.detectChanges();
    expect(submitButtonEl.textContent).toContain("Login");
    expect(toggleButtonEl.textContent).toContain("Switch to sign up");
  });

  describe('onSubmit', () => {
    let testForm = <NgForm>{
      valid: true,
      value: {
        email: 'valid@email.com',
        password: 'longenoughpassword',
      },
      reset: () => {
      }
    }

    it('should return undefined if the submit button is somehow clicked while the form is invalid', () => {
      //arrange
      let submitButtonEl = fixture.nativeElement.querySelector('#submit-button');
      // currently the form is blank and invalid
      //act and assert
      expect(click(submitButtonEl)).toBeUndefined();
    })

    it('should login and redirect a user to /recipes when loginMode===true and correct user data is provided', () => {
      // arrange
      let loginSpy = jest.spyOn(authService, 'login');
      let routerSpy = jest.spyOn(router, 'navigate').mockImplementation(() => of(true).toPromise());
      authCmp.loginMode = true;
      fixture.detectChanges();
      //act
      authCmp.onSubmit(testForm);
      //assert
      expect(loginSpy).toBeCalledWith(testForm.value.email, testForm.value.password);
      expect(routerSpy).toBeCalledWith(['/recipes']);
    });

    it('should signup and redirect a user to /recipes when loginMode===false and correct new user data is provided', () => {
      // arrange
      let signupSpy = jest.spyOn(authService, 'signup');
      let routerSpy = jest.spyOn(router, 'navigate').mockImplementation(() => of(true).toPromise());
      authCmp.loginMode = false;
      fixture.detectChanges();
      //act
      authCmp.onSubmit(testForm);
      //assert
      expect(signupSpy).toBeCalledWith(testForm.value.email, testForm.value.password);
      expect(routerSpy).toBeCalledWith(['/recipes']);
    });

    it('should create an alert component when a user tries to login with the wrong password and email information' , () => {
      //arrange
      let loginSpy = jest.spyOn(authService, 'login');
      let showErrorAlertSpy = jest.spyOn<any, string>(authCmp, "showErrorAlert");// passing generic to spyOn to bypass a private method
      authCmp.loginMode = true;
      fixture.detectChanges();
      //act
      testForm.value.email = 'incorrect test'
      authCmp.onSubmit(testForm);
      fixture.detectChanges();
      let alertBoxEl = fixture.debugElement.query(By.css('div.alert-box'));
      let viewContainer = fixture.debugElement.queryAllNodes(By.directive(PlaceholderDirective))[0];
      //assert
      expect(loginSpy).toBeCalledWith(testForm.value.email, testForm.value.password);
      expect(authCmp.error).toBe('Incorrect password');
      expect(showErrorAlertSpy).toBeCalledWith('Incorrect password');
      expect(authCmp.alertHost.viewContainerRef).toBeTruthy();
      expect(viewContainer.nativeNode).toBeTruthy();
      expect(alertBoxEl).toBeTruthy();
    });

    it('should create an alert component when someone tries to signup with an existing email' , () => {
      //arrange
      let signupSpy = jest.spyOn(authService, 'signup');
      let showErrorAlertSpy = jest.spyOn<any, string>(authCmp, "showErrorAlert");
      authCmp.loginMode = false;
      fixture.detectChanges();
      //act
      testForm.value.email = 'incorrect test'
      authCmp.onSubmit(testForm);
      fixture.detectChanges();
      let alertBoxEl = fixture.debugElement.query(By.css('div.alert-box'));
      let viewContainer = fixture.debugElement.queryAllNodes(By.directive(PlaceholderDirective))[0];
      //assert
      expect(signupSpy).toBeCalledWith(testForm.value.email, testForm.value.password);
      expect(authCmp.error).toBe('This email already exists');
      expect(showErrorAlertSpy).toBeCalledWith('This email already exists');
      expect(authCmp.alertHost.viewContainerRef).toBeTruthy();
      expect(viewContainer.nativeNode).toBeTruthy();
      expect(alertBoxEl).toBeTruthy();
    });
  })
})
