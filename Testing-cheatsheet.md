***If you are reading this within the SharePoint view, please download the file and open in an IDE or file viewer of
your choice. The SharePoint viewer does not fully support nesting lists and blockquotes, and the structure of this document may not be
properly conveyed. If you're reading this from the gitlab repo, the formatting should be preserved***

## Development and testing loops

### TDD:
1. Write a failing unit test.
2. Implement the code needed to pass the unit test.
3. Repeat steps 1 and 2 until you've built the feature/story.
4. Refactor unit test files
    1. Remember DRY, repeated data, and logic should be extracted into a helpers/utils style file.
5. Write the integration tests for the feature and it's relationships.
6. Check your code coverage.

### Development cycle:
1. Make a change.
2. compile the application.
3. Run tests
4. Run the application
5. Commit to CI/CD (testing, linting, building)

## Jest and Cypress testing tips for Angular:

### Jest and Cypress

* Jest/Jasmine and Cypress/Chai style tests are divided into `describe()` blocks, where `it()` blocks indicate a test
  themselves.
  * Within angular, the common testing block pattern is class name > method > tests.
  * Generally, if there is need for a `beforeEach()` for a group of tests, you should group those tests within
    a `describe` block.


* At the time I am writing this, a strategy on how to organize testing libraries has not been finalized, and there isn't
  a uniform consensus to be found online. Currently, all jest testing or *.spec.ts files should live in the directory
  with the classes they are testing. Cypress tests will be named with the *.cy.ts syntax and organized in their own
  directory at the root level. This will avoid the scoping issues mentioned [below](#jest-vs-cypress). The repeated
  logic, data, and mocks used by the **jest tests** should be housed within test-helper or test-util file, within a shared
  directory. A separate util/helper file should be created for Cypress, and housed within the cypress project directory.
  *  It's important to have a separated file for the cypress helpers, as importing angular libraries in **end to end or integration** tests can create webpack errors.
  * If our helper/util files begin to grow too large, or potentially run into scoping issues, it should be divided into
    an utils file per class directory. F.E, hypothetically two separate components need a different mock of the same
    service, here you would create multiple utils files.

### Jest

* 99% percent of your unit tests will declare/provide/import the angular class you are trying to test into the `TestBed`
  configuration, while you mock all its dependencies.
  * The `TestBed` configuration is the same as configuring an NgModule.
    * This setup will most often be done within the `beforeEach` block nested within your class (aka service, component,
      etc.) level describe block.
    * When you have dependencies to inject, most likely, each instance will be declared just with its TS type and a null
      value above your `beforeEach` block, and within your `beforeEach` block you will set the value of each dependency
      to `TestBed.inject(<dependency>)`.
  * Angular provides mocks for its own modules, this includes things like the HTTP client, the router module etc.
    * These two modules will likely be the ones used most in your tests. Here are the docs for
      the [RouterTestingModule](https://angular.io/api/router/testing/RouterTestingModule) and
      the [HttpClientTestingModule](https://angular.io/api/common/http/testing/HttpClientTestingModule).
    * When using the HttpClientTestingModule, include `afterEach(() => <instance of HttpClientTestingModule>.verify())`
      to ensure all requests have been flushed.
    * The mocked httpClient provided by the angular module will use the `expectOne('string url')` method to catch a
      mocked outgoing request, and the `flush(<response object>)` method to "send the request", and return a response.
    * When using the RouterTestingModule ensure that your router instance calls `router.initialNavigation()` within
      the `beforeEach` block that configures and injects it.


* Jest is built upon Jasmine, and uses many of the same keywords. The two main exceptions you will see while writing
  unit tests are:
  * matchers/assertions &ndash; logical checks to be performed on an expected value, more on this in
    the [Jest vs Cypress section](#jest-vs-cypress). Also see the matchers page within
    the [Jest docs](https://jestjs.io/docs/using-matchers).
  * Spies &ndash; Mocks that track metadata, most importantly when a function is called. Within the angular docs you
    will see spies written through Jasmine, to write a spy with Jest use the
    following: `jest.spyOn(<instance of the injected dependency>, <"method name as a string">)`.
    * Spies will be frequently used with the `.toBeCalledWith(arg, arg, ...)` jest matcher.


* While writing component tests with Jest, you will use
  the [componentFixture](https://angular.io/guide/testing-utility-apis#the-componentfixture)
  to interact with its fake DOM. Whenever you wish to update the items in this fake DOM, you must call
  the `detectChanges()` method of your componentFixture instance.

### Cypress

* Cypress is built upon Chai/Mocha, and uses the same keywords.
  * Cypress has multiple ways to write the assertions within your tests.
    * You can use the `expect()` or BDD style: `expect(add(1,2)).to.eq(3)`.
    * You can use the `assert` or TDD style: `assert.equal(add(1,2), 3, <"optional string message">)`.
      * [Docs](https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests#Assertion-Styles) for above
        styles
    * And lastly you can chain assertions on the cypress object. F.E. the following code would test that a p element
      within a div with class alert-box has the text "Incorrect password".
      `cy.get(".alert-box").within(() => cy.get("p").should('have.text', 'Incorrect password'));`
      * The first argument of the should method, is the chainer. This determines what check is being made. You can find
        the list of chainers [here]("https://docs.cypress.io/api/commands/should#Chainers").


* Mounting a component with Cypress will configure the `TestBed` behind the scenes, and you are able to access its instance to inject dependencies just like you can with Jest
  * You can also declare the values of your component's properties by passing an object to the `componentProperties` key within the mount config.


* Cypress enqueues all the commands called on the `cy` object.
  * This means Cypress does not support async await syntax. Instead, you can use a chained `then()`
    which accepts a callback function, or you can pass a callback function as an argument within a `should()` chain. Both of these callback functions behave like
    [native promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises).
  * Cypress does not allow you to return a promise from a command while also invoking one or more cy commands in that promise.
    * To oversimplify, this means you can't nest cy commands unless they are within specific chained commands. A common example of where you would think this is necessary is logging. 
    The cy object has a `log()` command that would be enqueued and will throw an error if you try to nest it. Use the synchronous `Cypress.log()` instead.
  * The following is an example from 
    [authorization e2e tests](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/cypress/e2e/auth.cy.ts). 
    This portion of the test ensures the user is successfully routed to the recipes page after logging in, and checks that the users data was saved to session storage.
    * ```typescript
      cy.url().should("include", '/recipes', () => {
        const userData = window.localStorage.getItem('userData');
        expect(authResponseDataGuard(userData)).to.be.true;
      });
      ```
    * If the `userData` was set outside a should or then callback block, it would return undefined, as JavaScript code is run before the enqueued cypress commands are run.
  * Here are the docs for [intercepts](https://docs.cypress.io/api/commands/intercept)
  and [network requests](https://docs.cypress.io/guides/guides/network-requests). 
  The intercept method of the cypress object is how you will handle, spy, and stub both Http responses and requests.
    * An intercept will listen for traffic on a specified url, but in order to access the response and request objects (stubbed or real)
    you will need to chain `its()` or call `wait()` on it's alias.
    The `its()` method will only allow you to chain matchers where calling the `wait()` will give you more freedom to interact with the intercepted object.
      * Aliases are created with `as(<alias name>)` method. An alias will prepend an `@` to the alias name.
    * Below is an example from [authorization e2e tests](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/cypress/e2e/auth.cy.ts)
    of an interceptor that is listening for a post request to our backend's Sign Up endpoint. It will handle the req and make an assertion about the response body.
    This example does 
      
      * ```typescript
        cy.intercept({url: urls.signup, method: "POST"}, (req) => {
          req.continue((res) => {
          expect(authResponseDataGuard(res.body)).to.be.true;
          });
        }).as('signUp');
        ```
      * This intercept's alias would be accessed with `cy.wait("@signUp")`


* It is best practice to add and set the data-cy attribute to equal a unique identifier for all components you will test
  in Cypress.
  This separates the identifier from JS behavioral changes and CSS styling.
  * If you need to dynamically bind the data-cy attribute use the `[attr.data-cy]` syntax within your html code .


* Cypress allows you to add additional chainable commands to the cy instance
  through `Cypress.Commands.Add('key name', fn)`.
  * This is used commonly in the setup of your e2e tests.
    Commands to spin up and tear down backend instances, and to seed and login a user are common place.
  * Cypress adds a commands.ts, and component.ts within the support directory on setup. 
  The commands.ts should contain the commands you are adding to the cypress object, and the component.ts
  is where you would edit the Cypress namespace to include the newly added commands for TypeScript.


## Jest vs Cypress

Jest and Cypress overlap in component testing, and in the keywords used to set up tests. If you run into any scoping
issues refer to
the [tsconfig.json](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/tsconfig.json)
, [package.json](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/package.json) in the project
root directory, and
the [cypress/tsconfig.json](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/cypress/tsconfig.json)
.

Jest has a **much faster** feedback loop than Cypress, and as such you should do as much component testing as you can
with it.

### When should I use Jest vs Cypress for component testing?

Jest completes component testing by creating a fake DOM representation and accessing it with the componentFixture.
> You can read more about how it's built and how to use
> it [here](https://angular.io/guide/testing-utility-apis#the-componentfixture).

This means you will not have a visual
representation of your component as you are testing it. This is the best litmus test for deciding between the two
libraries/platforms. If you would like to see changes in the DOM, or need to render additional components within a test
it's probably best to go with Cypress.

A great example of a component test that can be written with either library, but would be better represented in Cypress
is the `"should create an alert component when a user tries to login with the wrong password and email information"`
[test](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/src/app/auth/auth.component.ts).
The auth component has a `@ViewChild` that corresponds with a directive injected with the `ViewContainerRef`. This will
dynamically load components or HTML, which is a common pattern in angular.
Since the component is going to mount new components/HTML to the DOM, this makes a great case to be written with Cypress

### Jest vs Cypress syntax

Jest and Cypress use almost exactly the same syntax for structuring tests. They both separate testing blocks by
using `describe`, and `it` blocks. They use the same hook syntax in `beforeEach` and `afterEach`.
The biggest difference in syntax is the methods that `expect()` provides, aka matchers or assertions.
> [Docs](https://docs.cypress.io/guides/references/assertions) for Cypress matchers.
> [Docs](https://jestjs.io/docs/using-matchers) for Jest matchers.

> You may also see cypress tests written with `context()` and `specify()` blocks, these are equivalent to `describe()`
> and `it()` respectively. In much older versions of Mocha/Chai these aliases provided opportunities to add more context
> to tests, but this is no longer the case, and they are equivalent.

## Links to docs and tutorials:

* [Jest docs](https://jestjs.io/docs/getting-started)
* [Jest/Jasmine Angular testing docs](https://angular.io/guide/testing)
  * Documentation for components, services, directives, pipes and general debugging practices. Written for Jasmine not
    Jest, see [tips](#general-angular-testing-tips) section for info on this.
* [Cypress Angular component testing docs](https://docs.cypress.io/guides/component-testing/angular/overview)
* [Tutorial for testing guards](https://dev.to/this-is-angular/testing-angular-route-guards-with-the-routertestingmodule-45c9)
* [Tutorial for testing interceptors](https://medium.com/@dev.s4522/how-to-write-unit-test-cases-for-angular-http-interceptor-7595cb3a8843)
* [Repo with setup for testing in Jest and Cypress](https://github.com/cypress-io/cypress-and-jest-typescript-example)
* [Cypress and Cucumber setup for webpack preprocessor](https://github.com/badeball/cypress-cucumber-preprocessor)
* [Cypress element selecting best practices](https://docs.cypress.io/guides/references/best-practices#Selecting-Elements)
