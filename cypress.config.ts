import {defineConfig} from "cypress";
import * as webpack from "@cypress/webpack-preprocessor"

export default defineConfig({
  component: {
    devServer: {
      framework: "angular",
      bundler: "webpack",
    },
    specPattern: ["cypress/component/*.cy.ts"],
  },
  e2e: {
    specPattern: ["cypress/e2e/*.cy.ts"],
    baseUrl: "http://localhost:4200",
    setupNodeEvents(on: Cypress.PluginEvents, config: Cypress.PluginConfigOptions) {
    }
// implement node event listeners here
  }
});
