import type {Config} from "jest";

const jestConfig: Config = {
  globalSetup: 'jest-preset-angular/global-setup',
  roots: ["<rootDir>/src"],
  preset: 'jest-preset-angular',
  // moduleDirectories: ["<rootDir>/node_modules", "<rootDir>/src"],
  moduleDirectories: ["node_modules"],
  transform: {
    "^.+\\.(tsx|ts)?$": "jest-preset-angular"
  },
  setupFilesAfterEnv: ["<rootDir>/src/setup-jest.ts"],
  // testPathIgnorePatterns: ["<rootDir>/src/test.ts"]
}

export default jestConfig;
