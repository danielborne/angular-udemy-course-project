# AngularCourseProject

This project was built while
following [Maximilian Schwazrmuller's "Angular - The Complete Guide](https://www.udemy.com/course/the-complete-guide-to-angular-2/)
udemy course. The course covers the most recent version of angular (v15.1 as of when I'm writing this) and has you
create a recipe book app throughout it.

This project is meant to serve as an example artifact for any other Kinsale devs who take the course, as well as, a POC
for testing Angular with Cypress and Jest.

Please check out
the [cheat sheet](https://gitlab.com/danielborne/angular-udemy-course-project/-/blob/main/Testing-cheatsheet.html) for a
summary and tips on the Cypress and Jest platforms/libraries. There are unit tests, component tests, and integration/e2e
tests written for the authorization and recipes features of the app.

## Setup

1. After forking or cloning this repo ensure that you have the angular cli installed and install the project
   dependencies. If the version is not returned install the angular cli ahead of running the npm install.

``` bash
ng --version
npm install -g @angular/cli # run if you don't currently have angular/cli installed
npm install
```

3. Run unit tests with either of the following:

```bash
npm test <string pattern>
jest <string pattern>
```

> Jest will run the tests within files that match the pattern passed. F.E. within this project `npm test auth` would
> test all unit tests of the auth feature, while `npm test auth.c` would only run the testing suite for the auth
> component.

4. To run integration or component tests run

```bash
ng serve # wait for the server instance to spin up
cypress open
```

5. A client will launch that will have you select between integration/e2e test and component tests.
  1. You can change between the two options at any time, but will only be able to view one type of test at time.
6. Then it will have you choose which browser to run the tests in, and from there you can view any of the snapshots the
   cypress object creates by interacting with the runner in the browser.
