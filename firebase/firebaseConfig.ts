// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getDatabase} from "@firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "AIzaSyDV6Dh0Q2VjCt1J2Fegh3W7u0kdyzr1KXo",
  authDomain: "angular-course-project-14180.firebaseapp.com",
  databaseURL: "https://angular-course-project-14180-default-rtdb.firebaseio.com",
  projectId: "angular-course-project-14180",
  storageBucket: "angular-course-project-14180.appspot.com",
  messagingSenderId: "195419421559",
  appId: "1:195419421559:web:10af773a553682f0a2bf72",
  measurementId: "G-NZTCHF9QBQ"
};

// Initialize Firebase
export const firebaseApp = initializeApp(firebaseConfig);
export const analytics = getAnalytics(firebaseApp);
export const db = getDatabase(firebaseApp);
